@extends('layouts.app')

@section('content')
<section id="primary" class="content-full-width"> <!-- **Primary Starts Here** -->  
                      
    <div class="dt-sc-hr-invisible-small"></div>
    
    <div class="fullwidth-section"> <!-- **Full-width-section Starts Here** -->
        <div class="container">
            <div class="main-title animate" data-animation="pullDown" data-delay="100">
                <h2 class="aligncenter"> Portfolio </h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do </p>
            </div>
        </div>
        <div class="dt-sc-sorting-container">
            <a data-filter="*" href="#" title = "09" class="dt-sc-tooltip-top active-sort type1 dt-sc-button animate" data-animation="fadeIn" data-delay="100">All</a>
            <a data-filter=".nature" href="#" title = "06" class="dt-sc-tooltip-top type1 dt-sc-button animate" data-animation="fadeIn" data-delay="200">Nature</a>
            <a data-filter=".people" href="#" title = "06" class="dt-sc-tooltip-top type1 dt-sc-button animate" data-animation="fadeIn" data-delay="300">People</a>
            <a data-filter=".street" href="#" title = "05" class="dt-sc-tooltip-top type1 dt-sc-button animate" data-animation="fadeIn" data-delay="400">Street</a>
            <a data-filter=".still-life" href="#" title = "08" class="dt-sc-tooltip-top type1 dt-sc-button animate" data-animation="fadeIn" data-delay="500">Still life</a>
        </div>
        <div class="portfolio-fullwidth">
            <div class="portfolio-grid">
                <div class="dt-sc-portfolio-container isotope"> <!-- **dt-sc-portfolio-container Starts Here** -->
                    <div class="portfolio nature still-life dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new') }}/{{ $arts[0]->images[0]->url }}" alt="{{ $arts[0]->images[0]->alt }}" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/500x800&text=Portfolio+Image1"></a><span>3</span>
                                    </div>
                                    <div class="portfolio-title">
                                    <h5><a href="gallery-detail.html">{{ $arts[0]->name }} </a></h5>
                                        <p>{{ $arts[0]->description }}</p>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="portfolio nature people street dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new') }}/{{ $arts[1]->images[0]->url }}" alt="{{ $arts[1]->images[0]->alt }}" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/1250x1160&text=Portfolio+Image2"></a><span>7</span>
                                    </div>
                                    <div class="portfolio-title">
                                        <h5><a href="gallery-detail-with-lhs.html">{{ $arts[1]->name }}</a></h5>
                                        <p>{{ $arts[1]->description }}</p>
                                    </div>
                                </div>
                            </figcaption>
                       </figure>
                    </div>
                    <div class="portfolio street landscapes still-life dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new') }}/{{ $arts[2]->images[0]->url }}" alt="{{ $arts[2]->images[0]->alt }}" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/1300x900&text=Portfolio+Image3"></a><span>9</span>
                                    </div>
                                    <div class="portfolio-title">
                                        <h5><a href="gallery-detail-with-rhs.html">{{ $arts[2]->name }}</a></h5>
                                        <p>{{ $arts[2]->description }}</p>
                                    </div>
                                </div>
                            </figcaption>                                        
                       </figure>
                    </div>
                    <div class="portfolio nature still-life dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new/img-4.jpg') }}" alt="" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/700x600&text=Portfolio+Image4"></a><span>5</span>
                                    </div>
                                    <div class="portfolio-title">
                                        <h5><a href="gallery-detail.html">River Ride</a></h5>
                                        <p>Live Free, Ride Hard!!</p>
                                    </div>
                                </div>
                            </figcaption>                                        
                       </figure>
                    </div>
                    <div class="portfolio people still-life dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new/img-5.jpg') }}" alt="" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/1600x1300&text=Portfolio+Image5"></a><span>6</span>
                                    </div>
                                    <div class="portfolio-title">
                                        <h5><a href="gallery-detail-with-lhs.html">Ride Hard</a></h5>
                                        <p>Saddle your horse</p>
                                    </div>
                                </div>
                            </figcaption>                                        
                       </figure>
                    </div>
                    <div class="portfolio people still-life dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new/img-6.jpg') }}" alt="" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/1200x1500&text=Portfolio+Image6"></a><span>1</span>
                                    </div>
                                    <div class="portfolio-title">
                                        <h5><a href="gallery-detail-with-rhs.html">Black Rider</a></h5>
                                        <p>No reason to ride a horse</p>
                                    </div>
                                </div>
                            </figcaption>                                        
                       </figure>
                    </div>
                    <div class="portfolio people nature still-life street dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new/img-7.jpg') }}" alt="" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/1500x2000&text=Portfolio+Image7"></a><span>5</span>
                                    </div>
                                    <div class="portfolio-title">
                                        <h5><a href="gallery-detail.html">Stay single</a></h5>
                                        <p>Make Way</p>
                                    </div>
                                </div>
                            </figcaption>                                        
                       </figure>
                    </div>
                    <div class="portfolio nature people street still-life dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new/img-8.jpg') }}" alt="" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/1600x1100&text=Portfolio+Image8"></a><span>12</span>
                                    </div>
                                    <div class="portfolio-title">
                                        <h5><a href="gallery-detail-with-lhs.html">Born to Ride</a></h5>
                                        <p>Cowboys are born, ain't made!</p>
                                    </div>
                                </div>
                            </figcaption>                                        
                       </figure>
                    </div>
                    <div class="portfolio street nature people still-life dt-sc-one-fourth">
                        <figure>
                            <img src="{{ asset('assets/images/new/img-9.jpg') }}" alt="" title="">
                            <figcaption>
                                <div class="portfolio-detail">
                                    <div class="views">
                                        <a class="fa fa-camera-retro" data-gal="prettyPhoto[gallery]" href="http://placehold.it/650x450&text=Portfolio+Image9"></a><span>15</span>
                                    </div>
                                    <div class="portfolio-title">
                                        <h5><a href="gallery-detail-with-rhs.html">Stand Tall</a></h5>
                                        <p>Hard to Settle!!</p>
                                    </div>
                                </div>
                            </figcaption>                                        
                       </figure>
                    </div>
                </div><!-- **dt-sc-portfolio-container Ends Here** -->
            </div>
        </div>
    </div><!-- **Full-width-section Ends Here** -->
    <div class="clear"></div>              
    <div class="container">
        <div class="main-title animate" data-animation="pullDown" data-delay="100">
            <h2 class="aligncenter"> Blog </h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do </p>
        </div>
    </div>	
    <div class="fullwidth-section"><!-- **Full-width-section Starts Here** -->
        <div class="blog-section">
            <article class="blog-entry">
                <div class="entry-thumb">
                    <ul class="blog-slider">
                        <li> <img src="{{ asset('assets/images/new/blog-img1.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img2.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img3.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img4.jpg') }}" alt="" title=""> </li>
                    </ul>
                </div> 
                <div class="entry-details">
                    <div class="entry-title">
                        <h3><a href="blog-detail.html">Acrylic</a></h3>
                    </div>
                    <div class="entry-body">
                        <p><b>Acrylic painting</b>, technique in which pigments are mixed with hot, liquid wax. After all of the colours have been applied to the painting surface, a heating element is passed over them until the individual brush or spatula marks fuse into a uniform film. </p>
                    </div>
                    <a class="type1 dt-sc-button small" href="gallery-detail.html">View Gallery<i class="fa fa-angle-right"></i></a>
                </div>                   	
            </article>
        </div>
        
        <div class="blog-section">
            <article class="blog-entry type2">
                <div class="entry-details">
                    <div class="entry-title">
                        <h3><a href="blog-detail.html">Encaustic</a></h3>
                    </div>
                    <div class="entry-body">
                        <p><b>Encaustic painting</b>, technique in which pigments are mixed with hot, liquid wax. After all of the colours have been applied to the painting surface, a heating element is passed over them until the individual brush or spatula marks fuse into a uniform film. </p>
                    </div>
                    <a class="type1 dt-sc-button small" href="gallery-detail-with-lhs.html">View Gallery<i class="fa fa-angle-right"></i></a>
                </div>                   	
                <div class="entry-thumb">
                    <ul class="blog-slider">
                        <li> <img src="{{ asset('assets/images/new/blog-img5.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img6.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img7.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img8.jpg') }}" alt="" title=""> </li>
                    </ul>
                </div>                             
            </article>
        </div>
        
        <div class="blog-section">
            <article class="blog-entry">
                <div class="entry-thumb">
                    <ul class="blog-slider">
                        <li> <img src="{{ asset('assets/images/new/blog-img9.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img10.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img11.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img12.jpg') }}" alt="" title=""> </li>
                    </ul>
                </div> 
                <div class="entry-details">
                    <div class="entry-title">
                        <h3><a href="blog-detail-with-lhs.html">Oil painting</a></h3>
                    </div>
                    <div class="entry-body">
                        <p><b>Oil painting</b>, technique in which pigments are mixed with hot, liquid wax. After all of the colours have been applied to the painting surface, a heating element is passed over them until the individual brush or spatula marks fuse into a uniform film. </p>
                    </div>
                    <a class="type1 dt-sc-button small" href="gallery-detail-with-rhs.html">View Gallery<i class="fa fa-angle-right"></i></a>
                </div>                   	
            </article>
        </div>
        
        <div class="blog-section">
            <article class="blog-entry type2">
                <div class="entry-details">
                    <div class="entry-title">
                        <h3><a href="blog-detail-with-rhs.html">Impasto</a></h3>
                    </div>
                    <div class="entry-body">
                        <p><b>Impasto painting</b>, technique in which pigments are mixed with hot, liquid wax. After all of the colours have been applied to the painting surface, a heating element is passed over them until the individual brush or spatula marks fuse into a uniform film. </p>
                    </div>
                    <a class="type1 dt-sc-button small" href="gallery-detail-with-lhs.html">View Gallery<i class="fa fa-angle-right"></i></a>
                </div>                   	
                <div class="entry-thumb">
                    <ul class="blog-slider">
                        <li> <img src="{{ asset('assets/images/new/blog-img13.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img14.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img15.jpg') }}" alt="" title=""> </li>
                        <li> <img src="{{ asset('assets/images/new/blog-img16.jpg') }}" alt="" title=""> </li>
                    </ul>
                </div>                             
            </article>
        </div>
    </div><!-- **Full-width-section Ends Here** -->
    <div class="clear"></div>             
    
    <div class="fullwidth-section"><!-- **Full-width-section Starts Here** -->
        <div class="container">
            <div class="main-title animate" data-animation="pullDown" data-delay="100">
                <h2 class="aligncenter"> Frames </h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do </p>
            </div>
        </div>
        <div class="frame-grid"><!-- **Frame-Grid Starts Here** -->
            <div class="frame-thumb"><!-- **Frame-Thumb Starts Here** -->
                <div class="frame-fullwidth">
                    <div class="dt-sc-frame-container isotope"> <!-- **dt-sc-portfolio-container Starts Here** -->
                        <div class="frame ceramic dt-sc-one-third">
                            <figure>
                                <img src="http://placehold.it/272x185" alt="" title="">
                           </figure>
                        </div>
                        <div class="frame plastic dt-sc-one-third">
                            <figure>
                                <img src="http://placehold.it/200x250" alt="" title="">
                           </figure>
                        </div>
                        <div class="frame steel dt-sc-one-third">
                            <figure>
                                <img src="http://placehold.it/260x195" alt="" title="">
                           </figure>
                        </div>
                        <div class="frame wooden dt-sc-one-third">
                            <figure>
                                <img src="http://placehold.it/470x500" alt="" title="">
                           </figure>
                        </div>                                    
                        <div class="frame wooden dt-sc-one-third">
                            <figure>
                                <img src="http://placehold.it/375x300" alt="" title="">
                           </figure>
                        </div>
                    </div><!-- **dt-sc-portfolio-container Ends Here** -->
                </div>
            </div><!-- **Frame-Thumb Starts Here** -->
            <div class="frame-details"><!-- **Frame-Details Starts Here** -->
                <div class="frame-content">
                    <div id="frame-all" class="dt-frames">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                    </div>
                    <div id="frame-steel" class="dt-frames hidden">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                    </div>
                    <div id="frame-wooden" class="dt-frames hidden">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                    <div id="frame-plastic" class="dt-frames hidden">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div id="frame-ceramic" class="dt-frames hidden">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                    </div>
                </div>
                <div class="frame-sorting"><!-- **Frame-sorting Starts Here** -->
                    <a data-filter="*" href="#" class="active-sort type1 dt-sc-button animate" data-animation="fadeIn" data-delay="100">All</a>
                    <a data-filter=".steel" href="#" class="type1 dt-sc-button animate" data-animation="fadeIn" data-delay="200">Steel</a>
                    <a data-filter=".wooden" href="#" class="type1 dt-sc-button animate" data-animation="fadeIn" data-delay="300">Wooden</a>
                    <a data-filter=".plastic" href="#" class="type1 dt-sc-button animate" data-animation="fadeIn" data-delay="400">plastic</a>
                    <a data-filter=".ceramic" href="#" class="type1 dt-sc-button animate" data-animation="fadeIn" data-delay="500">Ceramic</a>
                </div><!-- **Frame-sorting Ends Here** -->
            </div><!-- **Frame-Details Ends Here** -->
        </div><!-- **Frame-Grid Ends Here** -->
    </div><!-- **Full-width-section Ends Here** -->
    
    <div class="dt-sc-hr-invisible-small"></div>
    <div class="clear"></div>

    <div class="fullwidth-section"><!-- **Full-width-section Starts Here** -->
        <div class="container">
        
            <div class="main-title animate" data-animation="pullDown" data-delay="100">
                <h2 class="aligncenter"> About Me </h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do </p>
            </div>
            
            <div class="about-section">
            
                <div class="dt-sc-one-half column first">
                    <img src="{{ asset('asstes/images/new/about.png') }}" title="" alt="">
                </div>
                
                <div class="dt-sc-one-half column">
                    <h3 class="animate" data-animation="fadeInLeft" data-delay="200"> A Little Intro</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                    <h3 class="animate" data-animation="fadeInLeft" data-delay="300">My Exhibitions</h3>
                    <p>Sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Lorem ipsum dolor quis nostrud exercitation ullamco</p>
                    <h3 class="animate" data-animation="fadeInLeft" data-delay="400">Newsletter</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
                    <form method="post" class="mailchimp-form dt-sc-three-fourth" name="frmnewsletter" action="php/subscribe.php">	
                        <p class="input-text">
                           <input class="input-field" type="email" name="mc_email" value="" required/>
                           <label class="input-label">
                                    <i class="fa fa-envelope-o icon"></i>
                                    <span class="input-label-content">Mail</span>
                                </label>
                           <input type="submit" name="submit" class="submit" value="Subscribe" />
                        </p>
                    </form>
                    <div id="ajax_subscribe_msg"></div>                               
                </div>
            </div>
        </div>
    </div><!-- **Full-width-section Ends Here** -->
    
    <div class="dt-sc-hr-invisible-small"></div>
    
</section><!-- **Primary Ends Here** -->
@endsection