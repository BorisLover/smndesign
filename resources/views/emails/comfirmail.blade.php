@component('mail::message')
# Introduction
# Dear, {{$content['name']}}

The body of your message.
You are receiving this email because we received a signup request for your this mail account.


@component('mail::button', ['url' => 'website.com'])
Click here
@endcomponent

If you did not request a signup , no further action is required.

Thanks,<br>
{{ config('app.name') }}
@endcomponent


