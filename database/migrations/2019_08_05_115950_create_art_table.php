<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArtTable extends Migration {

	public function up()
	{
		Schema::create('art', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name', 200);
			$table->string('description', 200);
			$table->integer('price');
			$table->integer('quantity');
			$table->time('time');
			$table->string('technique', 255);
			$table->string('equipment', 255);
			$table->string('support', 255);
		});
	}

	public function down()
	{
		Schema::drop('Art');
	}
}