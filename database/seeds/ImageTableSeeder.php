<?php

use App\Models\Art;
use App\Models\Image;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i=1;
        while( $i<21){
            $image = new Image([
                'url' => 'img-'.$i.'.jpg',
                'alt' => Str::random(10)
            ]);

            $art = Art::find($i);
            $art->images()->save($image);
            $i++;
        }
    }
}
