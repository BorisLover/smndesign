<?php

use App\Models\Art;
use App\Models\Image;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ArtTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i=1;
        while( $i<21){
            $art = new Art([
                'name' => Str::random(10),
                'description' => Str::random(10),
                'price' => random_int(3, 100),
                'quantity' => random_int(1, 10),
                'time' => $i * 150,
                'technique' => Str::random(20),
                'equipment' => Str::random(20),
                'support' => Str::random(20),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            $art->save();
            $i++;
        }
    }
}
