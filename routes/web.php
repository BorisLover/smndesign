<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('index');
}); */

Route::resource('user', 'UserController');
Route::resource('art', 'ArtController');
Route::resource('image', 'ImageController');
Route::resource('event', 'EventController');
Route::resource('video', 'VideoController');
Route::get('sendemail', 'SendEmailController@index')->name('sendemail');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');

Route::prefix('admin')->group(function () {
    Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
});
