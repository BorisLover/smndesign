<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Rendering the homepage of the dashboard
     */
    public function dashboard()
    {
        return view('dashboard.index');
    }
}
