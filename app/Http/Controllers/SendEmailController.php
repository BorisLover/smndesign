<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ComfirmationMail;
use App\Http\Controllers\Controller;

class SendEmailController extends Controller
{
    /**
     * Ship the given order.
     *
     * @param  Request  $request
     * @param  int  $orderId
     * @return Response
     */
    public function ship(Request $request)
    {
        $valueArray = [
        	'name' => 'John',
        ];

        // Test mail...

        try {
            Mail::to('example@gmail.com')->send(new ComfirmationMail($valueArray));
            echo 'Mail send successfully';
        } catch (\Exception $e) {
            echo 'Error - '.$e;
        }
    }
}
