<?php

namespace App\Http\Controllers;

use App\Models\Art;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $arts = Art::with('images')->get();

        /* foreach ($arts[0]->images as $image) {
            dd($image);
        } */
        // dd($arts[0]->images());
        return view('index', compact('arts'));
    }

}
