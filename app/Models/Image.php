<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model 
{

    protected $table = 'Image';
    public $timestamps = true;
    protected $fillable = array('url', 'alt');

    public function imageable()
    {
        return $this->morphTo();
    }

}