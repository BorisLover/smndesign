<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Art extends Model 
{

    protected $table = 'Art';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'price', 'quantity', 'time', 'technique', 'equipment', 'support');

    public function videos()
    {
        return $this->hasMany('App\Models\Video', 'art_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps()->withPivot('quantity', 'sell_price');
    }

}